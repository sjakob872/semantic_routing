#include "asp/ASPParser.h"
#include <algorithm>
#include <string>

namespace sr
{
const std::string ASPParser::pathBegin = "path(";
const std::string ASPParser::uuidBegin = "uuid(";
const std::string ASPParser::doubleNegation = "not -";
const std::string ASPParser::distBegin = "dist(";

std::string ASPParser::getNodeRepresentation(const std::string& node)
{
    return "node(uuid(" + node + "))";
}

std::string ASPParser::getNodeExternal(const std::string& node)
{
    return "#external " + ASPParser::getNodeRepresentation(node) + ".";
}

std::string ASPParser::getIndividualRepresentation(const std::string& path, const std::string& uuid, int dist)
{
    return "route(" + path + ",uuid(" + uuid + "),dist(" + std::to_string(dist) + "),uuid(" + uuid + "))";
}

std::string ASPParser::getIndividualExternal(const std::string& path, const std::string& uuid, int dist)
{
    return "#external " + ASPParser::getIndividualRepresentation(path, uuid, dist) + ".";
}

std::vector<std::string> ASPParser::split(const std::string& toSplit, const std::string& delimiter)
{
    std::vector<std::string> split;
    size_t start = 0;
    size_t end = toSplit.find(delimiter);
    while (end != std::string::npos) {
        split.push_back(toSplit.substr(start, end - start));
        start = end + delimiter.length();
        end = toSplit.find(delimiter, start);
    }
    split.push_back(toSplit.substr(start, toSplit.size() - start));
    return split;
}

std::string ASPParser::getBaseClass(const std::string& first, const std::string& second)
{
    std::vector<std::string> firstSplit = ASPParser::split(first);
    std::vector<std::string> secondSplit = ASPParser::split(second);
    if (secondSplit.size() > firstSplit.size()) {
        return "";
    }
    int numberOfEqualBaseClasses = 0;
    for (int i = 0; i < firstSplit.size() && i < secondSplit.size(); i++) {
        if (secondSplit.at(i) != firstSplit.at(i)) {
            break;
        }
        numberOfEqualBaseClasses = i;
    }
    numberOfEqualBaseClasses += 1;
    if (numberOfEqualBaseClasses == secondSplit.size()) {
        return "";
    }
    std::string ret = "path(";
    for (int i = 0; i < numberOfEqualBaseClasses; i++) {
        ret += secondSplit.at(i);
        if (i + 1 < numberOfEqualBaseClasses) {
            ret += ',';
        }
    }
    ret += ')';
    return ret;
}

std::string ASPParser::getPath(const std::string& route)
{
    size_t posPath = route.find(ASPParser::pathBegin);
    if (posPath == std::string::npos) {
        return "";
    }
    size_t posCLosingBrace = route.find_first_of(')', posPath + ASPParser::pathBegin.size());
    return route.substr(posPath + ASPParser::pathBegin.size(), posCLosingBrace - posPath - ASPParser::pathBegin.size());
}

std::string ASPParser::getPathPredicate(const std::string& route)
{
    size_t posPath = route.find(ASPParser::pathBegin);
    if (posPath == std::string::npos) {
        return "";
    }
    size_t posCLosingBrace = route.find_first_of(')', posPath + ASPParser::pathBegin.size());
    return route.substr(posPath, posCLosingBrace - posPath + 1);
}

std::string ASPParser::getExternal(const std::string& predicate)
{
    return "#external " + predicate + ".";
}

std::string ASPParser::getRouteRule(const std::string& path, const std::string& aggregator, int dist, const std::string& receivedFrom)
{
    return ASPParser::getRoute(path, aggregator, dist, receivedFrom) + " :- " + ASPParser::getNodeRepresentation(receivedFrom) + ", not " +
           ASPParser::getNegativeRoute(path, aggregator, dist, receivedFrom) + ".";
}

std::string ASPParser::getRoute(const std::string& path, const std::string& aggregator, int dist, const std::string& receivedFrom)
{
    return "route(" + path + ",uuid(" + aggregator + "),dist(" + std::to_string(dist) + "),uuid(" + receivedFrom + "))";
}

std::string ASPParser::getNegativeRouteExternal(
        const std::string& path, const std::string& aggregator, int dist, const std::string& receivedFrom)
{
    return "#external " + ASPParser::getNegativeRoute(path, aggregator, dist, receivedFrom) + ".";
}

std::string ASPParser::getNegativeRoute(const std::string& path, const std::string& aggregator, int dist, const std::string& receivedFrom)
{
    return "-" + ASPParser::getRoute(path, aggregator, dist, receivedFrom);
}

std::string ASPParser::getNegation(const std::string& route)
{
    return "-" + route;
}

std::string ASPParser::getRuleHead(const std::string& rule)
{
    size_t indicationPos = rule.find(":-");
    if (indicationPos == std::string::npos) {
        if (rule.find('#') != std::string::npos) {
            size_t posBlanc = rule.find(' ');
            return trim(rule.substr(posBlanc + 1, rule.size() - posBlanc - 2));
        } else {
            return trim(rule);
        }
    } else {
        return trim(rule.substr(0, indicationPos - 1));
    }
}
std::string ASPParser::trim(const std::string& str)
{
    const std::string& whitespace = " \t";
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos) {
        return ""; // no content
    }
    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

std::string ASPParser::adaptRuleOrigin(const std::string& rule, const std::string& uuid)
{
    size_t firstUUID = rule.find(ASPParser::uuidBegin);
    size_t secondUUID = rule.find(ASPParser::uuidBegin, firstUUID + ASPParser::uuidBegin.size());
    size_t closingSecondUUID = rule.find(')', secondUUID);
    size_t thirdUUID = rule.find(ASPParser::uuidBegin, secondUUID + ASPParser::uuidBegin.size());
    size_t closingThirdUUID = rule.find(')', thirdUUID);
    size_t fourthUUID = rule.find(ASPParser::uuidBegin, thirdUUID + ASPParser::uuidBegin.size());
    size_t fifthUUID = rule.find(ASPParser::uuidBegin, fourthUUID + ASPParser::uuidBegin.size());
    size_t closingFifthUUID = rule.find(')', fifthUUID);
    std::string ret = rule.substr(0, secondUUID + ASPParser::uuidBegin.size()) + uuid +
                      rule.substr(closingSecondUUID, thirdUUID - closingSecondUUID + ASPParser::uuidBegin.size()) + uuid +
                      rule.substr(closingThirdUUID, fifthUUID - closingThirdUUID + ASPParser::uuidBegin.size()) + uuid +
                      rule.substr(closingFifthUUID, rule.size() - closingFifthUUID);
    return ret;
}

std::string ASPParser::adaptRuleOriginIndividual(const std::string& rule, const std::string& uuid)
{
    size_t firstUUID = rule.find(ASPParser::uuidBegin);
    size_t secondUUID = rule.find(ASPParser::uuidBegin, firstUUID + ASPParser::uuidBegin.size());
    size_t closingSecondUUID = rule.find(')', secondUUID);
    std::string ret = rule.substr(0, secondUUID + ASPParser::uuidBegin.size()) + uuid +
                      rule.substr(closingSecondUUID, rule.size() - closingSecondUUID);
    return ret;
}

std::string ASPParser::adaptRuleNegativeBody(const std::string& rule, const std::string& uuid)
{
    size_t doubleNegationPos = rule.find(ASPParser::doubleNegation);
    std::string ret = rule.substr(
            doubleNegationPos + ASPParser::doubleNegation.size() - 1, rule.size() - doubleNegationPos - ASPParser::doubleNegation.size());
    size_t firstUUID = ret.find(ASPParser::uuidBegin);
    size_t secondUUID = ret.find(ASPParser::uuidBegin, firstUUID + ASPParser::uuidBegin.size());
    return ret.substr(0, secondUUID + ASPParser::uuidBegin.size()) + uuid + "))";
}

std::string ASPParser::getLocation(const std::string& ruleHead)
{
    size_t posUUID = ruleHead.find(ASPParser::uuidBegin);
    if (posUUID == std::string::npos) {
        return "";
    }
    size_t posCLosingBrace = ruleHead.find_first_of(')', posUUID + ASPParser::uuidBegin.size());
    return ruleHead.substr(posUUID + ASPParser::uuidBegin.size(), posCLosingBrace - posUUID - ASPParser::uuidBegin.size());
}

int ASPParser::getDistance(const std::string& ruleHead)
{
    size_t posPath = ruleHead.find(ASPParser::distBegin);
    if (posPath == std::string::npos) {
        return -1;
    }
    size_t posCLosingBrace = ruleHead.find_first_of(')', posPath + ASPParser::distBegin.size());
    return stoi(ruleHead.substr(posPath + ASPParser::uuidBegin.size(), posCLosingBrace - posPath + ASPParser::uuidBegin.size()));
}

std::string ASPParser::getAggregator(const std::string& ruleHead)
{
    size_t posFirstUUID = ruleHead.find(ASPParser::uuidBegin);
    size_t posSecondUUID = ruleHead.find(ASPParser::uuidBegin, posFirstUUID + ASPParser::uuidBegin.size());
    if (posFirstUUID == std::string::npos) {
        return "";
    }
    size_t posCLosingBrace = ruleHead.find_first_of(')', posSecondUUID + ASPParser::uuidBegin.size());
    return ruleHead.substr(posSecondUUID + ASPParser::uuidBegin.size(), posCLosingBrace - posSecondUUID - ASPParser::uuidBegin.size());
}

std::string ASPParser::increaseDist(const std::string& rule)
{
    size_t firstDist = rule.find(ASPParser::distBegin);
    size_t closingFirstDist = rule.find(')', firstDist + ASPParser::distBegin.size());
    int firstDistance =
            std::stoi(rule.substr(firstDist + ASPParser::distBegin.size(), closingFirstDist - firstDist - ASPParser::distBegin.size())) + 1;
    size_t secondDist = rule.find(ASPParser::distBegin, closingFirstDist + 1);
    size_t closingSecondDist = rule.find(')', secondDist + ASPParser::distBegin.size());
    int secondDistance =
            std::stoi(rule.substr(secondDist + ASPParser::distBegin.size(), closingSecondDist - secondDist - ASPParser::distBegin.size())) +
            1;
    return rule.substr(0, firstDist + ASPParser::distBegin.size()) + std::to_string(firstDistance) +
           rule.substr(closingFirstDist, secondDist - closingFirstDist + ASPParser::distBegin.size()) + std::to_string(secondDistance) +
           rule.substr(closingSecondDist, rule.size() - closingSecondDist);
}

std::string ASPParser::increaseDistIndividual(const std::string& rule)
{
    size_t firstDist = rule.find(ASPParser::distBegin);
    size_t closingFirstDist = rule.find(')', firstDist + ASPParser::distBegin.size());
    int firstDistance =
            std::stoi(rule.substr(firstDist + ASPParser::distBegin.size(), closingFirstDist - firstDist - ASPParser::distBegin.size())) + 1;
    return rule.substr(0, firstDist + ASPParser::distBegin.size()) + std::to_string(firstDistance) +
           rule.substr(closingFirstDist, rule.size() - closingFirstDist);
}

std::string ASPParser::expandToRule(const std::string& head)
{
    std::string ret;
    ret.append(head)
            .append(" :- node(uuid(")
            .append(ASPParser::getAggregator(head))
            .append(")), not ")
            .append(ASPParser::getNegation(head))
            .append(".");
    return ret;
}

std::string ASPParser::extractExternal(const std::string& external)
{
    std::string ret;
    size_t posStartExternal = external.find(" ");
    ret = external.substr(posStartExternal + 1, external.size() - posStartExternal - 2);
    return ret;
}

bool ASPParser::isIndividual(const std::string& str)
{
    std::string tmp = ASPParser::getPath(str);
    if (tmp.empty()) {
        return false;
    }
    size_t posLastComma = tmp.find_last_of(',');
    tmp = tmp.substr(posLastComma + 1, tmp.size() - posLastComma - 1);
    tmp.erase(std::remove(tmp.begin(), tmp.end(), '\"'), tmp.end());
    return (!tmp.empty() && std::isupper(tmp[0]));
}

bool ASPParser::isRule(const std::string& str)
{
    return str.find(":-") != std::string::npos;
}

bool ASPParser::isSuperOrEqualClass(const std::string& first, const std::string& second)
{
    auto firstSplit = ASPParser::split(ASPParser::getPath(first));
    auto secondSplit = ASPParser::split(ASPParser::getPath(second));
    for (int i = 0; i < firstSplit.size() && i < secondSplit.size(); i++) {
        if (firstSplit.at(i) != secondSplit.at(i)) {
            return false;
        }
    }
    return true;
}

} // namespace sr
