#include "Message.h"

#include "Node.h"
#include <sstream>

namespace sr
{
Message::Message() {
    this->receiver = nullptr;
}
std::ostream& operator<<(std::ostream& os, const sr::Message& msg)
{
    os << "Message from " << msg.origin << " to " << msg.receiver->getUuid() << std::endl;
    os << "Routes: " << std::endl;
    for (const auto& route : msg.routingEntries) {
        os << "\t" << route.routingRule << std::endl;
    }
    os << "External to true: " << std::endl;
    for (const auto& route : msg.externalsToTrue) {
        os << "\t" << route << std::endl;
    }
    os << "External to false: " << std::endl;
    for (const auto& route : msg.externalsToFalse) {
        os << "\t" << route << std::endl;
    }
    return os;
}
} // namespace sr
