#include "Node.h"
#include "NodeType.h"
#include "model/Query.h"

#include <cstdlib>
#include <vector>
#include <iostream>

void rescueScenario() {
    std::vector<sr::Node*> nodes;
    sr::Node* rescuers = new sr::Node("rescuers", sr::NodeType::Rescuers);
    nodes.push_back(rescuers);
    sr::Node* uav1 = new sr::Node("uav1", sr::NodeType::UAV);
    nodes.push_back(uav1);
    sr::Node* uav2 = new sr::Node("uav2", sr::NodeType::UAV);
    nodes.push_back(uav2);
    sr::Node* uav3 = new sr::Node("uav3", sr::NodeType::UAV);
    nodes.push_back(uav3);
    sr::Node* streetLight1 = new sr::Node("streetLight1", sr::NodeType::StreetLight);
    nodes.push_back(streetLight1);
    sr::Node* streetLight2 = new sr::Node("streetLight2", sr::NodeType::StreetLight);
    nodes.push_back(streetLight2);
    sr::Node* robot1 = new sr::Node("robot1", sr::NodeType::Robot);
    nodes.push_back(robot1);
    sr::Node* robot2 = new sr::Node("robot2", sr::NodeType::Robot);
    nodes.push_back(robot2);
    sr::Node* smartphone1 = new sr::Node("smartphone1", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone1);
    sr::Node* smartphone2 = new sr::Node("smartphone2", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone2);
    sr::Node* smartphone3 = new sr::Node("smartphone3", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone3);
    sr::Node* smartphone4 = new sr::Node("smartphone4", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone4);

    rescuers->addNeighbour(streetLight2);
    streetLight2->addNeighbour(uav1);
    streetLight2->addNeighbour(robot1);
    uav1->addNeighbour(streetLight1);
    streetLight1->addNeighbour(uav2);
    uav2->addNeighbour(smartphone4);
    uav2->addNeighbour(robot1);
    robot1->addNeighbour(smartphone2);
    robot1->addNeighbour(robot2);
    smartphone2->addNeighbour(smartphone3);
    robot2->addNeighbour(smartphone1);
    smartphone1->addNeighbour(uav3);

    auto start = std::chrono::high_resolution_clock::now();
    smartphone3->addIndividual("path(\"human\",\"person\",\"patient\",\"C\")");
    smartphone2->addIndividual("path(\"human\",\"person\",\"patient\",\"B\")");
    smartphone1->addIndividual("path(\"human\",\"person\",\"patient\",\"A\")");
    smartphone4->addIndividual("path(\"human\",\"person\",\"D\")");
    auto finish = std::chrono::high_resolution_clock::now();
    auto generationDuration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    start = std::chrono::high_resolution_clock::now();
    sr::Query* query = new sr::Query("path(\"human\",\"person\",\"patient\",\"C\")", rescuers);
    rescuers->query(query);
    finish = std::chrono::high_resolution_clock::now();
    auto query1Duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    delete query;

    start = std::chrono::high_resolution_clock::now();
    sr::Query* query2 = new sr::Query("path(\"human\",\"person\",\"patient\")", rescuers);
    rescuers->query(query2);
    finish = std::chrono::high_resolution_clock::now();
    auto query2Duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    delete query2;

    start = std::chrono::high_resolution_clock::now();
    sr::Query* query3 = new sr::Query("path(\"human\",\"person\")", rescuers);
    rescuers->query(query3);
    finish = std::chrono::high_resolution_clock::now();
    auto query3Duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    delete query3;

    for(sr::Node* node : nodes) {
        delete node;
    }

    std::string nodeWithMostMessagesSent;
    std::string nodeWithMostMessagesReceived;
    int totalMessages = 0;
    for(sr::Node* node : nodes) {
        totalMessages += node->getSentMessages();
    }

    std::stringstream stream;
    stream << "/home/stefan/krws/src/semantic_routing/etc/test-scenario.txt";
    auto outfile = std::ofstream(stream.str(), std::ios_base::app);
    outfile << generationDuration << ";" << query1Duration << ";" << query2Duration << ";" << query3Duration << ";" << totalMessages << ";" << (double)totalMessages / (double)nodes.size() << std::endl;
}

int main(int argc, char** argv)
{
    rescueScenario();
    return EXIT_SUCCESS;
}