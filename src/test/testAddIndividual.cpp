#include "Node.h"

#include <cstdlib>
#include <iostream>
#include <iomanip>

int main(int argc, char** argv)
{
    int id;
    if(argc == 1) {
        id = 1;
    } else {
        id = std::stoi(argv[1]);
    }

    //auto time = std::time(nullptr);
    std::stringstream stream;
    stream << "/home/stefan/krws/src/semantic_routing/etc/test-" << id << ".txt";
    //stream << "/home/stefan/krws/src/semantic_routing/etc/test-addIndividual" << "-" << std::put_time(gmtime(&time), "%F_%T") << ".txt";

    auto outfile = new std::ofstream(stream.str(), std::ios_base::app);
    *outfile << "Aggregate;Update;Total" << std::endl;

    std::vector<std::string> paths = {"path(\"human\",\"person\",\"patient\",\"", "path(\"human\",\"person\",\"", "path(\"human\",\""};
    std::string endString = "\")";

    auto robot1 = new sr::Node("robot1", sr::NodeType::Robot);
    robot1->setLoggingFile(outfile);

    std::chrono::high_resolution_clock::time_point start;
    std::chrono::high_resolution_clock::time_point finish;
    long duration;
    for(int i = 0; i < 50; ++i) {
        start = std::chrono::high_resolution_clock::now();
        robot1->addIndividual(paths.at(0) + "A" + std::to_string(i) + endString);
        finish = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
        *outfile << (double) duration / 1000.0 << std::endl;
    }
    for(int i = 0; i < 50; ++i) {
        start = std::chrono::high_resolution_clock::now();
        robot1->addIndividual(paths.at(1) + "B" + std::to_string(i) + endString);
        finish = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
        *outfile << (double) duration / 1000.0 << std::endl;
    }
    for(int i = 0; i < 50; ++i) {
        start = std::chrono::high_resolution_clock::now();
        robot1->addIndividual(paths.at(2) + "C" + std::to_string(i) + endString);
        finish = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
        *outfile << (double) duration / 1000.0 << std::endl;
    }

    outfile->close();
    delete robot1;
    delete outfile;

    return EXIT_SUCCESS;
}
