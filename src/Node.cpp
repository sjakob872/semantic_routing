#include "Node.h"
#include "asp/ASPParser.h"
#include "model/Query.h"
#include "model/RoutingEntry.h"
#include "model/RoutingTable.h"

#include <sstream>
#include <utility>

const std::string sr::Node::NULL_VALUE = "NULL";
const std::string sr::Node::SKIP_VALUE = "SKIP";

namespace sr
{

Node::Node(std::string  uuid, NodeType type)
        : uuid(std::move(uuid))
        , type(type)
        , logFile(nullptr)
{
    this->neighbours = new std::map<std::string, Node*>();
    this->solver = new reasoner::asp::Solver({});
    this->solver->add("", {}, ("node(uuid(" + this->uuid + ")).").c_str());
    this->solver->ground({{"", {}}}, nullptr);
    this->solver->solve();
    this->psCounter = 0;
    this->sentMessages = 0;
    this->receivedMessages = 0;
    this->routingTable = new RoutingTable(this);
}

Node::~Node()
{
    delete this->neighbours;
    delete this->solver;
}

void Node::addNeighbour(Node* node)
{
    if (!this->neighbours->empty() && this->neighbours->find(node->getUuid()) != this->neighbours->end()) {
        return;
    }
    this->neighbours->emplace(node->getUuid(), node);
    this->solver->add(node->getUuid().c_str(), {}, (ASPParser::getNodeExternal(node->getUuid()).c_str()));
    this->solver->ground({{node->getUuid().c_str(), {}}}, nullptr);
    if (node->getNodeExternal().empty()) {
        node->setNodeExternal(ASPParser::ASPParser::getNodeRepresentation(node->getUuid()));
    }
    this->solver->assignExternal(this->solver->parseValue(node->getNodeExternal()), Clingo::TruthValue::True);
    this->solver->solve();
    node->addNeighbour(this);
}

void Node::addIndividual(const std::string& path)
{
    RoutingEntry entry =
            RoutingEntry(ASPParser::getIndividualExternal(path, this->uuid), ASPParser::getNodeExternal(this->uuid), Node::NULL_VALUE);
#ifdef LOGGING
    auto start = std::chrono::high_resolution_clock::now();
#endif
    this->updateTable(entry, Node::NULL_VALUE);
#ifdef LOGGING
    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    *this->logFile << (double) duration / 1000.0 << ";" << std::flush;
#endif
    /*if (!updated) {
        propagateIndividual(entry);
    }*/
}

void Node::propagateIndividual(const RoutingEntry& entry)
{
    for (const auto& neighbour : *neighbours) {
        if (!maySendMessage(neighbour.second, entry.routingRule)) {
            continue;
        }

        RoutingEntry e =
                RoutingEntry(ASPParser::getRouteRule(ASPParser::getPathPredicate(entry.routingRule),
                                     ASPParser::getLocation(entry.routingRule), ASPParser::getDistance(entry.routingRule) + 1, this->uuid),
                        ASPParser::getNodeExternal(uuid),
                        ASPParser::getNegativeRouteExternal(ASPParser::getPathPredicate(entry.routingRule),
                                ASPParser::getLocation(entry.routingRule), ASPParser::getDistance(entry.routingRule) + 1, this->uuid));
        Message msg;
        msg.routingEntries.push_back(e);
        msg.externalsToTrue.push_back(ASPParser::getNodeRepresentation(this->uuid));
        send(msg, neighbour.second);
    }
}

void Node::propagate(const std::string& routingRule, bool aggregated)
{
    if (aggregated) {
        for (const auto& neighbour : *this->neighbours) {
            if (!maySendMessage(neighbour.second, routingRule)) {
                continue;
            }
            std::string negativeRuleBody = ASPParser::adaptRuleNegativeBody(routingRule, this->uuid);
            negativeRuleBody = ASPParser::increaseDistIndividual(negativeRuleBody);
            std::string rule = ASPParser::adaptRuleOrigin(routingRule, this->uuid);
            rule = ASPParser::increaseDist(rule);
            RoutingEntry entry = RoutingEntry(rule, ASPParser::getNodeExternal(this->uuid),
                    ASPParser::getExternal(negativeRuleBody));
            Message m;
            m.routingEntries.push_back(entry);
            m.externalsToTrue.push_back(ASPParser::getNodeRepresentation(this->uuid));
            m.externalsToFalse.push_back(negativeRuleBody);
            send(m, neighbour.second);
        }
    } else {
        for (const auto& neighbour : *this->neighbours) {
            if (!maySendMessage(neighbour.second, routingRule)) {
                continue;
            }
            // std::string aggregator = ASPParser::getAggregator(ASPParser::getRuleHead(msg.routingEntries.at(0).routingRule));
            std::string rule = ASPParser::increaseDist(routingRule);
            rule = ASPParser::adaptRuleOrigin(rule, this->uuid);
            std::string negativeBody = ASPParser::adaptRuleNegativeBody(rule, this->uuid);
            RoutingEntry entry =
                    RoutingEntry(rule, ASPParser::getNodeExternal(ASPParser::getLocation(rule)), ASPParser::getExternal(negativeBody));
            Message m;
            m.routingEntries.push_back(entry);
            m.externalsToTrue.push_back(ASPParser::getNodeRepresentation(ASPParser::getLocation(rule)));
            m.externalsToFalse.push_back(negativeBody);
            send(m, neighbour.second);
        }
    }
}

void Node::send(Message& msg, Node* receiver)
{
    this->sentMessages++;
    msg.origin = this->uuid;
    msg.receiver = receiver;
    receiver->receive(msg);
}

void Node::receive(const Message& msg)
{
    this->receivedMessages++;
    handleMessage(msg);
}

void Node::updateTable(const RoutingEntry& entry, const std::string& extUUID)
{
    if (extUUID != Node::NULL_VALUE) {
        this->solver->assignExternal(this->solver->parseValue(this->neighbours->at(extUUID)->getNodeExternal()), Clingo::TruthValue::False);
        this->solver->solve();
        return;
    }
    bool aggregated = false;
    if (entry.routingRule != Node::NULL_VALUE) {
#ifdef LOGGING
        auto start = std::chrono::high_resolution_clock::now();
#endif
        auto aggregationPair = aggregate(entry.routingRule);
#ifdef LOGGING
        auto finish = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
        *this->logFile << (double) duration / 1000.0 << ";" << std::flush;
#endif
        if (aggregationPair.first != Node::NULL_VALUE && aggregationPair.first != Node::SKIP_VALUE) {
            if (!aggregationPair.second.empty()) {
                for(const auto& route : aggregationPair.second) {
                    this->solver->assignExternal(
                            this->solver->parseValue(ASPParser::getNegation(route)), Clingo::TruthValue::True);
                }
                applyChanges();
            }
            if (isNewRoute(aggregationPair.first)) {
                std::string ps = "ps" + std::to_string(this->psCounter);
                std::string pgrm;
                if (aggregationPair.first.find(":-") == std::string::npos) {
                    pgrm.append(ASPParser::expandToRule(aggregationPair.first));
                } else {
                    pgrm.append(aggregationPair.first);
                }
                pgrm.append("\n");
                pgrm.append("#external " + ASPParser::getNegation(ASPParser::getRuleHead(aggregationPair.first)) + ".");
                this->solver->add(ps.c_str(), {}, (pgrm.c_str()));
                this->solver->ground({{ps.c_str(), {}}}, nullptr);
                applyChanges();
                std::string tmp = aggregationPair.first;
                if (!ASPParser::isRule(tmp)) {
                    tmp = ASPParser::expandToRule(aggregationPair.first);
                    tmp = ASPParser::increaseDist(tmp);
                }
                propagate(tmp, true);
            }
            aggregated = true;
            if (!ASPParser::isIndividual(entry.routingRule)) {
                return;
            }
        }
        if(aggregationPair.first == Node::SKIP_VALUE) {
            return;
        }
        if (isNewRoute(entry.routingRule)) {
            std::string ps = "ps" + std::to_string(this->psCounter);
            this->solver->add(ps.c_str(), {}, entry.routingRule.c_str());
            if (entry.nodeExternal != Node::NULL_VALUE && isNewRoute(entry.nodeExternal)) {
                this->solver->add(ps.c_str(), {}, entry.nodeExternal.c_str());
            }
            if (entry.negativeExternal != Node::NULL_VALUE) {
                this->solver->add(ps.c_str(), {}, entry.negativeExternal.c_str());
            }
            this->solver->ground({{ps.c_str(), {}}}, nullptr);
            if (ASPParser::isIndividual(entry.routingRule)) {
                this->solver->assignExternal(solver->parseValue(ASPParser::getRuleHead(entry.routingRule)), Clingo::TruthValue::True);
            }
            if (entry.nodeExternal != Node::NULL_VALUE) {
                this->solver->assignExternal(solver->parseValue(ASPParser::getRuleHead(entry.nodeExternal)), Clingo::TruthValue::True);
            }
            applyChanges();
            if (!ASPParser::isIndividual(entry.routingRule)) {
                propagate(entry.routingRule, false);
            } else {
                if(!aggregated) {
                    propagateIndividual(entry);
                }
            }
        }
    }
}

std::pair<std::string, std::vector<std::string>> Node::aggregate(const std::string& route)
{
    std::string aggregatedRoute = Node::NULL_VALUE;
    std::vector<std::string> removeRoutes;
    std::string path;
    for (const auto& entry : this->routingTable->getRoutingTable()) {
        std::string first = ASPParser::getPath(route);
        std::string second = ASPParser::getPath(entry);
        if (second.empty()) {
            continue;
        }
        if (first == second && ASPParser::getLocation(route) == ASPParser::getLocation(entry)) {
            if (ASPParser::getDistance(route) >= ASPParser::getDistance(entry)) {
                return std::make_pair(Node::SKIP_VALUE, removeRoutes);
            } else {
                aggregatedRoute = route;
                removeRoutes.push_back(entry);
                continue;
            }
        }
        if (ASPParser::isIndividual(route) && ASPParser::isSuperOrEqualClass(entry, route)) {
            aggregatedRoute = ASPParser::getRoute(ASPParser::getPathPredicate(entry), this->uuid, 0, this->uuid);
            return std::make_pair(aggregatedRoute, removeRoutes);
        }
        path = ASPParser::getBaseClass(second, first);
        if (!path.empty()) {
            aggregatedRoute = ASPParser::getRoute(path, this->uuid, 0, this->uuid);
            if (!ASPParser::isIndividual(entry)) {
                removeRoutes.push_back(entry);
            }
            return std::make_pair(aggregatedRoute, removeRoutes);
        }
    }
    return std::make_pair(aggregatedRoute, removeRoutes);
}

bool Node::isNewRoute(const std::string& route) const
{
    if (route.find("#external -") != std::string::npos) {
        return false;
    }
    for (const auto& entry : this->routingTable->getRoutingTable()) {
        std::string ruleHead = ASPParser::getRuleHead(route);
        if (entry == ruleHead) {
            return false;
        }
        if (entry.find("node(") == 0) {
            continue;
        }
        std::string firstAggregator = ASPParser::getAggregator(ruleHead);
        std::string secondAggregator = ASPParser::getAggregator(entry);
        std::string firstPath = ASPParser::getPath(ruleHead);
        std::string secondPath = ASPParser::getPath(entry);
        int firstDist = ASPParser::getDistance(ruleHead);
        int secondDist = ASPParser::getDistance(entry);
        if (firstAggregator == secondAggregator && firstDist >= secondDist && firstPath == secondPath) {
            return false;
        }
    }
    return true;
}

void Node::applyChanges()
{
    this->solver->solve();
    this->psCounter++;
    this->routingTable->setRoutingTable(this->solver->getCurrentModels());
}

void Node::handleMessage(const Message& msg)
{
    for (const auto& routingEntry : msg.routingEntries) {
        updateTable(routingEntry, Node::NULL_VALUE);
    }
    for (const std::string& ext : msg.externalsToTrue) {
        this->solver->assignExternal(solver->parseValue(ext), Clingo::TruthValue::True);
    }
    for (const std::string& ext : msg.externalsToFalse) {
        this->solver->assignExternal(solver->parseValue(ext), Clingo::TruthValue::False);
    }
    this->solver->solve();
    this->routingTable->setRoutingTable(this->solver->getCurrentModels());
}

void Node::query(Query* query)
{
    for (const auto& nodePair : *this->neighbours) {
        sendQuery(query, nodePair.second);
    }
}

void Node::sendQuery(Query* query, Node* receiver)
{
    query->addNodeToPath(receiver);
    query->increaseMessages();
    receiver->receiveQuery(query);
}

void Node::receiveQuery(Query* query)
{
    for (const auto& entry : this->routingTable->getRoutingTable()) {
        if(entry.find('-') != std::string::npos) {
            continue;
        }
        std::string pathPredicate = ASPParser::getPathPredicate(entry);
        if ((ASPParser::isIndividual(query->getQueryString()) && query->getQueryString() == pathPredicate) ||
                (ASPParser::isSuperOrEqualClass(query->getQueryString(), pathPredicate))) {
            if (ASPParser::getDistance(entry) > 0) {
                auto receiver = this->neighbours->at(ASPParser::getAggregator(entry));
                if (query->alreadyQueried(receiver)) {
                    continue;
                }
                query->addQueried(this);
                sendQuery(query, receiver);
            } else {
                if (ASPParser::isIndividual(entry)) {
                    query->addResult(entry);
                }
                returnQuery(query);
                if(ASPParser::isIndividual(query->getQueryString())) {
                    break;
                }
            }
        }
    }
    query->addQueried(this);
}

void Node::returnQuery(Query* query)
{
    Node* node = query->popNodeFromPath();
    if (node == nullptr) {
        return;
    } else {
        node->returnQuery(query);
    }
}

bool Node::maySendMessage(Node* node, const std::string& routingRule)
{
    std::string ruleHead = ASPParser::getRuleHead(routingRule);
    if (node->getUuid() == ASPParser::getAggregator(ruleHead)) {
        return false;
    }
    if (node->getType() == Rescuers) {
        return false;
    }
    if (node->getUuid() == ASPParser::getLocation(ruleHead)) {
        return false;
    }
    if (node->getUuid() == ASPParser::getAggregator(ruleHead)) {
        return false;
    }
    return true;
}

const std::string& Node::getUuid() const
{
    return this->uuid;
}

NodeType Node::getType() const
{
    return this->type;
}

const std::string& Node::getNodeExternal() const
{
    return this->nodeExternal;
}

void Node::setNodeExternal(const std::string& ne)
{
    this->nodeExternal = ne;
}

int Node::getReceivedMessages() const
{
    return this->receivedMessages;
}

int Node::getSentMessages() const
{
    return this->sentMessages;
}

RoutingTable* Node::getRoutingTable() const
{
    return this->routingTable;
}

std::ostream& operator<<(std::ostream& os, const Node& node)
{
    os << "UUID: " << node.uuid << std::endl;
    os << "Type: " << nodeType[node.type] << std::endl;
    os << "Neighbours: ";
    for (const auto& pair : *node.neighbours) {
        os << pair.first << " ";
    }
    os << std::endl;
    os << "Number of received messages: " << node.receivedMessages << std::endl;
    os << "Number of sent messages: " << node.sentMessages << std::endl;
    node.solver->solve();
    os << "Routing Table: " << std::endl;
    auto models = node.solver->getCurrentModels();
    for (auto& model : models) {
        for (auto atom : model) {
            os << "\t" << atom << std::endl;
        }
    }
    return os;
}

void Node::setLoggingFile(std::ofstream* file)
{
    this->logFile = file;
}

} // namespace sr