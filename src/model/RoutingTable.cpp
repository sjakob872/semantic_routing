#include "model/RoutingTable.h"

#include "Node.h"

namespace sr
{

RoutingTable::RoutingTable(Node* node)
        : node(node)
{
}

const std::vector<std::string>& RoutingTable::getRoutingTable() const
{
    return this->routingTable;
}

void RoutingTable::setRoutingTable(std::vector<Clingo::SymbolVector> currentModels)
{
    this->routingTable.clear();
    for (const auto& model : currentModels) {
        for (const auto& atom : model) {
            this->routingTable.push_back(atom.to_string());
        }
    }
}

std::ostream& operator<<(std::ostream& os, const RoutingTable& table)
{
    os << "Routing Table of: " << table.node->getUuid() << std::endl;
    os << "Table Entries: ";
    for (const auto& entry : table.getRoutingTable()) {
        os << "\t" << entry << "\n";
    }
    os << std::endl;
    return os;
}
} // namespace sr