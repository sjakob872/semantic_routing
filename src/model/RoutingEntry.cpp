#include "model/RoutingEntry.h"

sr::RoutingEntry::RoutingEntry(std::string routingRule, std::string nodeExternal, std::string negativeExternal)
        : routingRule(routingRule)
        , nodeExternal(nodeExternal)
        , negativeExternal(negativeExternal)
{
}

sr::RoutingEntry::RoutingEntry()
        : receiver(nullptr)
{
}
