#include "model/Query.h"

#include "Node.h"

namespace sr
{
Query::Query(std::string queryString, Node* origin)
        : queryString(queryString)
        , origin(origin->getUuid())
{
    this->path.push_back(origin);
    this->numberOfMessages = 0;
}

const std::string& sr::Query::getQueryString() const
{
    return this->queryString;
}

const std::vector<Node*>& sr::Query::getHistory() const
{
    return this->path;
}

void Query::addNodeToPath(Node* node)
{
    this->path.push_back(node);
}

Node* Query::popNodeFromPath()
{
    if (this->path.empty()) {
        return nullptr;
    }
    Node* ret = this->path.back();
    this->path.pop_back();
    this->visited.push_back(ret);
    return ret;
}

const std::vector<std::string>& Query::getResults() const
{
    return this->results;
}

void Query::addResult(const std::string& result)
{
    this->results.push_back(result);
}

const std::string& Query::getOrigin() const
{
    return this->origin;
}

std::ostream& operator<<(std::ostream& os, const sr::Query& query)
{
    os << "Query from: " << query.getOrigin() << "\n";
    os << "\tQuery String: " << query.getQueryString() << "\n";
    os << "\tResults: "
       << "\n";
    for (auto result : query.getResults()) {
        os << "\t\t" << result << "\n";
    }
    os << "\tPath: \n\t\t";
    for (Node* node : query.getVisited()) {
        os << node->getUuid() << " ";
    }
    os << "Number of Messages: " << query.numberOfMessages << "\n";
    return os;
}

const std::vector<Node*>& Query::getVisited() const
{
    return this->visited;
}

bool Query::alreadyVisited(Node* node)
{
    return std::find(this->visited.begin(), this->visited.end(), node) != this->visited.end();
}

void Query::addQueried(Node* node)
{
    if (std::find(this->kbQueried.begin(), this->kbQueried.end(), node) != this->kbQueried.end()) {
        return;
    }
    this->kbQueried.push_back(node);
}

bool Query::alreadyQueried(Node* node)
{
    return std::find(this->kbQueried.begin(), this->kbQueried.end(), node) != this->kbQueried.end();
}

void Query::increaseMessages() {
    this->numberOfMessages++;
}

} // namespace sr