#include "Node.h"
#include "NodeType.h"
#include "model/Query.h"

#include <cstdlib>
#include <vector>
#include <iostream>

void evaluate(std::vector<sr::Node *> &nodes, long duration);
void rescueScenario();
void minimalSetup();

int main(int argc, char** argv)
{
    rescueScenario();
    minimalSetup();
    return EXIT_SUCCESS;
}

void rescueScenario() {
    std::vector<sr::Node*> nodes;
    sr::Node* rescuers = new sr::Node("rescuers", sr::NodeType::Rescuers);
    nodes.push_back(rescuers);
    sr::Node* uav1 = new sr::Node("uav1", sr::NodeType::UAV);
    nodes.push_back(uav1);
    sr::Node* uav2 = new sr::Node("uav2", sr::NodeType::UAV);
    nodes.push_back(uav2);
    sr::Node* uav3 = new sr::Node("uav3", sr::NodeType::UAV);
    nodes.push_back(uav3);
    sr::Node* streetLight1 = new sr::Node("streetLight1", sr::NodeType::StreetLight);
    nodes.push_back(streetLight1);
    sr::Node* streetLight2 = new sr::Node("streetLight2", sr::NodeType::StreetLight);
    nodes.push_back(streetLight2);
    sr::Node* robot1 = new sr::Node("robot1", sr::NodeType::Robot);
    nodes.push_back(robot1);
    sr::Node* robot2 = new sr::Node("robot2", sr::NodeType::Robot);
    nodes.push_back(robot2);
    sr::Node* smartphone1 = new sr::Node("smartphone1", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone1);
    sr::Node* smartphone2 = new sr::Node("smartphone2", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone2);
    sr::Node* smartphone3 = new sr::Node("smartphone3", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone3);
    sr::Node* smartphone4 = new sr::Node("smartphone4", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone4);

    rescuers->addNeighbour(streetLight2);
    streetLight2->addNeighbour(uav1);
    streetLight2->addNeighbour(robot1);
    uav1->addNeighbour(streetLight1);
    streetLight1->addNeighbour(uav2);
    uav2->addNeighbour(smartphone4);
    uav2->addNeighbour(robot1);
    robot1->addNeighbour(smartphone2);
    robot1->addNeighbour(robot2);
    smartphone2->addNeighbour(smartphone3);
    robot2->addNeighbour(smartphone1);
    smartphone1->addNeighbour(uav3);

    auto start = std::chrono::high_resolution_clock::now();
    smartphone3->addIndividual("path(\"human\",\"person\",\"patient\",\"C\")");
    smartphone2->addIndividual("path(\"human\",\"person\",\"patient\",\"B\")");
    smartphone1->addIndividual("path(\"human\",\"person\",\"patient\",\"A\")");
    smartphone4->addIndividual("path(\"human\",\"person\",\"D\")");
    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    evaluate(nodes, duration);

    std::cout << std::endl;
    sr::Query* query = new sr::Query("path(\"human\",\"person\",\"patient\",\"C\")", rescuers);
    rescuers->query(query);
    std::cout << "Query successfully answered: \n\t" << *query << std::endl;
    delete query;

    std::cout << std::endl;
    sr::Query* query2 = new sr::Query("path(\"human\",\"person\",\"patient\")", rescuers);
    rescuers->query(query2);
    std::cout << "Query successfully answered: \n\t" << *query2 << std::endl;
    delete query2;

    std::cout << std::endl;
    sr::Query* query3 = new sr::Query("path(\"human\",\"person\")", rescuers);
    rescuers->query(query3);
    std::cout << "Query successfully answered: \n\t" << *query3 << std::endl;
    delete query3;

    for(sr::Node* node : nodes) {
        delete node;
    }
}

void minimalSetup() {
    std::vector<sr::Node*> nodes;
    sr::Node* smartphone1 = new sr::Node("smartphone1", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone1);
    sr::Node* smartphone2 = new sr::Node("smartphone2", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone2);
    sr::Node* smartphone3 = new sr::Node("smartphone3", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone3);
    sr::Node* smartphone4 = new sr::Node("smartphone4", sr::NodeType::SmartPhone);
    nodes.push_back(smartphone4);
    sr::Node* uav1 = new sr::Node("uav1", sr::NodeType::UAV);
    nodes.push_back(uav1);
    sr::Node* uav2 = new sr::Node("uav2", sr::NodeType::UAV);
    nodes.push_back(uav2);
    sr::Node* rescuers = new sr::Node("rescuers", sr::NodeType::Rescuers);
    nodes.push_back(rescuers);

    smartphone1->addNeighbour(smartphone2);
    smartphone2->addNeighbour(smartphone3);
    smartphone3->addNeighbour(smartphone4);
    uav1->addNeighbour(smartphone2);
    uav1->addNeighbour(smartphone3);
    uav2->addNeighbour(uav1);
    smartphone4->addNeighbour(rescuers);

    auto start = std::chrono::high_resolution_clock::now();
    smartphone1->addIndividual("path(\"human\",\"person\",\"patient\",\"C\")");
    smartphone2->addIndividual("path(\"human\",\"person\",\"patient\",\"A\")");
    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    evaluate(nodes, duration);

    std::cout << std::endl;
    sr::Query* query = new sr::Query("path(\"human\",\"person\",\"patient\",\"C\")", rescuers);
    rescuers->query(query);
    std::cout << "Query successfully answered: \n\t" << *query << std::endl;
    delete query;

    std::cout << std::endl;
    sr::Query* query2 = new sr::Query("path(\"human\",\"person\",\"patient\")", rescuers);
    rescuers->query(query2);
    std::cout << "Query successfully answered: \n\t" << *query2 << std::endl;
    delete query2;

    std::cout << std::endl;
    sr::Query* query3 = new sr::Query("path(\"human\",\"person\")", rescuers);
    rescuers->query(query3);
    std::cout << "Query successfully answered: \n\t" << *query3 << std::endl;
    delete query3;

    for(sr::Node* node : nodes) {
        delete node;
    }
}

void evaluate(std::vector<sr::Node *> &nodes, long duration) {
    //blank line after SystemConfig output
    std::cout << std::endl;
    std::string nodeWithMostMessagesSent;
    int highestSentNumber = -1;
    std::string nodeWithMostMessagesReceived;
    int highestReceivedNumber = -1;
    int totalMessages = 0;
    for(sr::Node* node : nodes) {
        totalMessages += node->getSentMessages();
        if(node->getReceivedMessages() > highestReceivedNumber) {
            highestReceivedNumber = node->getReceivedMessages();
            nodeWithMostMessagesReceived = node->getUuid();
        }
        if(node->getSentMessages() > highestSentNumber) {
            highestSentNumber = node->getSentMessages();
            nodeWithMostMessagesSent = node->getUuid();
        }
        std::cout << *node << std::endl;
    }

    std::cout << "################################################################################" << std::endl;
    std::cout << "\tPropagation took " << duration / 1000.0 << " ms." << std::endl;
    std::cout << "\t" << totalMessages << " Messages were sent in total, which are " << (double)totalMessages / (double)nodes.size() << " on average per node." << std::endl;
    std::cout << "\tNode " << nodeWithMostMessagesSent << " sent the most messages, which are " << highestSentNumber << std::endl;
    std::cout << "\tNode " << nodeWithMostMessagesReceived << " received the most messages, which are " << highestReceivedNumber << std::endl;
    std::cout << "################################################################################" << std::endl;
}