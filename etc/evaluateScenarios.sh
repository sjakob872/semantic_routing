#!/bin/sh
cd $HOME/krws/devel/lib/semantic_routing/

for i in {0..100}
	do
    	echo "Measurement $i / 100 for Circle"
    	./semantic_routing_circle_test
done

for i in {0..100}
	do
    	echo "Measurement $i / 100 for Grid"
    	./semantic_routing_grid_test
done

for i in {0..100}
	do
    	echo "Measurement $i / 100 for Line"
    	./semantic_routing_line_test
done

for i in {0..100}
	do
    	echo "Measurement $i / 100 for Scenario"
    	./semantic_routing_scenario_test
done
