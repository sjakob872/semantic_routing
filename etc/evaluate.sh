#!/bin/sh

file=$HOME/krws/src/semantic_routing/etc/combinedResults-0.txt
if [ -e $file ]; then
	echo "File $file already exists!"
	exit
else
	command > $file
fi

cd $HOME/krws/devel/lib/semantic_routing/

for i in {0..100}
	do
    	echo "Measurement $i / 100 for "
    	./semantic_routing_update_test $i
done

cd $HOME/krws/src/semantic_routing/etc/


for i in {0..100}
	do
		paste -d ";" combinedResults-$i.txt test-$i.txt > combinedResults-$((i+1)).txt
		rm combinedResults-$i.txt
done
