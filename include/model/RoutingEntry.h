#pragma once

#include <string>

namespace sr
{
class Node;
class RoutingEntry
{

public:
    RoutingEntry();
    RoutingEntry(std::string routingRule, std::string nodeExternal, std::string negativeExternal);

    std::string routingRule;
    std::string nodeExternal;
    std::string negativeExternal;

    std::string origin;
    Node* receiver;
};

} // namespace sr