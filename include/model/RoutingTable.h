#pragma once

#include <clingo.hh>
#include <string>
#include <vector>

namespace sr
{
class Node;
class RoutingTable
{
public:
    explicit RoutingTable(Node* node);

    const std::vector<std::string>& getRoutingTable() const;
    void setRoutingTable(std::vector<Clingo::SymbolVector> currentModels);
    friend std::ostream& operator<<(std::ostream& os, const sr::RoutingTable& table);

protected:
    Node* node;
private:
    std::vector<std::string> routingTable;
};
} // namespace sr
