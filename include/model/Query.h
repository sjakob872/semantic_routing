#pragma once

#include <iostream>
#include <string>
#include <vector>

namespace sr
{
class Node;

class Query
{
public:
    explicit Query(std::string queryString, Node* origin);
    const std::string& getQueryString() const;
    const std::vector<Node*>& getHistory() const;
    void addNodeToPath(Node* node);
    Node* popNodeFromPath();
    const std::vector<std::string>& getResults() const;
    void addResult(const std::string& result);
    const std::string& getOrigin() const;
    const std::vector<Node*>& getVisited() const;
    bool alreadyVisited(Node* node);
    bool alreadyQueried(Node* node);
    void addQueried(Node* node);
    void increaseMessages();
    friend std::ostream& operator<<(std::ostream& os, const sr::Query& query);

private:
    std::string queryString;
    std::vector<std::string> results;
    std::string origin;
    std::vector<Node*> path;
    std::vector<Node*> kbQueried;
    std::vector<Node*> visited;
    int numberOfMessages;
};
} // namespace sr