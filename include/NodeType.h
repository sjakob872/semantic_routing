#pragma once
namespace sr
{

#define UNUSED_VAR __attribute__((unused))

#define SR_TYPES(TYPE)                                                                                                                     \
    TYPE(Rescuers)                                                                                                                         \
    TYPE(StreetLight)                                                                                                                      \
    TYPE(UAV)                                                                                                                              \
    TYPE(Robot)                                                                                                                            \
    TYPE(SmartPhone)

// Enum
#define MAKE_ENUM(VAR) VAR,

enum NodeType
{
    SR_TYPES(MAKE_ENUM)
};

// Strings
#define MAKE_STRINGS(VAR) #VAR,
UNUSED_VAR static const char* nodeType[] = {SR_TYPES(MAKE_STRINGS)};

} // namespace sr