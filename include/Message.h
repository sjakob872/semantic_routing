#pragma once

#include "model/RoutingEntry.h"

#include <string>
#include <vector>

namespace sr
{
class Node;
class Message
{
public:
    Message();
    std::string origin;
    Node* receiver;
    std::vector<RoutingEntry> routingEntries;
    std::vector<std::string> externalsToTrue;
    std::vector<std::string> externalsToFalse;

    friend std::ostream& operator<<(std::ostream& os, const sr::Message& msg);
};
} // namespace sr
