#pragma once

#include "Message.h"
#include "NodeType.h"
#include "model/RoutingEntry.h"

#include <map>
#include <reasoner/asp/Solver.h>
#include <string>

//#define LOGGING

namespace sr
{
class RoutingTable;
class Query;
class Node
{
public:
    Node(std::string  uuid, NodeType type);
    ~Node();
    void addNeighbour(Node* node);

    void addIndividual(const std::string& path);
    void query(Query* query);

    const std::string& getUuid() const;
    NodeType getType() const;
    RoutingTable* getRoutingTable() const;

    const std::string& getNodeExternal() const;
    void setLoggingFile(std::ofstream* logFile);
    int getReceivedMessages() const;
    int getSentMessages() const;
    friend class Query;
    friend std::ostream& operator<<(std::ostream& os, const sr::Node& node);

private:
    unsigned long long psCounter;
    reasoner::asp::Solver* solver;
    std::string uuid;
    NodeType type;
    std::map<std::string, Node*>* neighbours;
    std::string nodeExternal;
    int receivedMessages;
    int sentMessages;
    RoutingTable* routingTable;
    std::ofstream* logFile;
    static const std::string NULL_VALUE;
    static const std::string SKIP_VALUE;

    /*
     * Main Steps
     */
    void updateTable(const RoutingEntry& entry, const std::string& extUUID = Node::NULL_VALUE);
    std::pair<std::string, std::vector<std::string>> aggregate(const std::string& route);
    void propagateIndividual(const RoutingEntry& entry);

    /*
     * Message Handling
     */
    void handleMessage(const Message& msg);
    void receive(const Message& msg);
    void send(Message& msg, Node* receiver);
    void propagate(const std::string& routingRule, bool aggregated);

    /*
     * Query
     */
    static void sendQuery(Query* query, Node* receiver);
    void receiveQuery(Query* query);
    static void returnQuery(Query* query);

    /*
     * Utility Methods
     */
    void applyChanges();
    bool isNewRoute(const std::string& route) const;
    static bool maySendMessage(Node* node, const std::string& routingRule);
    void setNodeExternal(const std::string& nodeExternal);
};
} // namespace sr
