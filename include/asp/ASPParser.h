#pragma once

#include <string>
#include <vector>

namespace sr
{
class ASPParser
{
public:
    ASPParser() = default;
    static std::string getNodeRepresentation(const std::string& node);
    static std::string getNodeExternal(const std::string& node);
    static std::string expandToRule(const std::string& head);

    static std::string getIndividualRepresentation(const std::string& path, const std::string &uuid, int dist = 0);
    static std::string getIndividualExternal(const std::string& path, const std::string &uuid, int dist = 0);

    static std::string getPath(const std::string& route);
    static std::string getPathPredicate(const std::string& route);
    static std::string getBaseClass(const std::string& first, const std::string& second);
    static std::string getLocation(const std::string& ruleHead);
    static int getDistance(const std::string& ruleHead);
    static std::string getAggregator(const std::string& ruleHead);

    static std::string getExternal(const std::string& predicate);
    static std::string extractExternal(const std::string& external);

    static std::string getRoute(const std::string& path, const std::string &aggregator, int dist, const std::string& receivedFrom);
    static std::string getRouteRule(const std::string& path, const std::string &aggregator, int dist, const std::string& receivedFrom);
    static std::string getNegativeRoute(const std::string& path, const std::string &aggregator, int dist, const std::string& receivedFrom);
    static std::string getNegativeRouteExternal(const std::string& path, const std::string &aggregator, int dist, const std::string& receivedFrom);
    static std::string getRuleHead(const std::string& rule);

    static std::string adaptRuleOrigin(const std::string& rule, const std::string& uuid);
    static std::string adaptRuleOriginIndividual(const std::string& rule, const std::string& uuid);
    static std::string adaptRuleNegativeBody(const std::string& rule, const std::string& uuid);
    static std::string increaseDist(const std::string& rule);
    static std::string increaseDistIndividual(const std::string& rule);
    static std::string getNegation(const std::string& route);

    static std::vector<std::string> split(const std::string& toSplit, const std::string& delimiter =",");
    static std::string trim(const std::string& str);

    static bool isIndividual(const std::string& str);
    static bool isRule(const std::string& str);
    static bool isSuperOrEqualClass(const std::string& first, const std::string& second);

private:
    static const std::string pathBegin;
    static const std::string uuidBegin;
    static const std::string doubleNegation;
    static const std::string distBegin;
};
} // namespace sr
